clc
clear
load('TemperatureData.mat');
xx=Data';
x=xx*(Max-Min)+Min;
train_input=[ones(1,1800);xx(1,1:1800)];
train_output=xx(1,2:1801);
train_output_real=x(1,2:1801);
test_input=[ones(1,1200);xx(1,1801:3000)];
test_output=xx(1802:3001);
test_output_real=x(1802:3001);

inputnodes=2;
reservoirnodes=200;
outputnodes=1;
spectral=0.9;
lam=10^(-8);

for j=1:30
    tic
    w=rand(5,reservoirnodes);
    input_weight=(2*rand(reservoirnodes,inputnodes)-1)*0.5;
    temp_internalWeights=sprand(reservoirnodes,reservoirnodes,0.05);
    Max_Value=(max(abs(eigs(temp_internalWeights,1))));
    internal_Weights=spectral*(temp_internalWeights/Max_Value);
    output_weight=zeros(outputnodes,reservoirnodes+inputnodes);
    internal_qiyi_value(j)=max(sqrt(abs(eigs(internal_Weights'*internal_Weights))));
    %训练集
    for t=1:1800
        if t==1
            x0=input_weight*train_input(:,t)+internal_Weights*zeros(reservoirnodes,1);
            f=tanh(x0);
            f_1=tanh(x0-1);
            f1=tanh(x0+1);
            f_2=tanh(x0-2);
            f2=tanh(x0+2);
            output_reservoir(:,t)=w(1,:)'.*f+w(2,:)'.*f_1+w(3,:)'.*f1+w(4,:)'.*f_2+w(5,:)'.*f2;
        else
            x1=input_weight*train_input(:,t)+internal_Weights*output_reservoir(:,t-1);
            f=tanh(x1);
            f_1=tanh(x1-1);
            f1=tanh(x1+1);
            f_2=tanh(x1-2);
            f2=tanh(x1+2);
            output_reservoir(:,t)=w(1,:)'.*f+w(2,:)'.*f_1+w(3,:)'.*f1+w(4,:)'.*f_2+w(5,:)'.*f2;
        end
    end
    A=max(output_reservoir,[],2);
    B=min(output_reservoir,[],2);
    output_reservoir=((output_reservoir-B)./(A-B))*2-1;
    res_state=[train_input;output_reservoir];
    res_state=res_state(:,301:1800);
    output_weight=(res_state*res_state'+lam*eye(reservoirnodes+inputnodes))\res_state*train_output(:,301:1800)';
    realout_G=output_weight'*res_state;
    realout_G=realout_G*(max(x)-min(x))+min(x);
    NRMSE_train_G(j)=sqrt(sum((realout_G-train_output_real(:,301:1800)).^2)/(1500*var(train_output_real(:,301:1800))));
TrainTime=toc;
%测试集
    for t=1:1200
        if t==1
            test_x0=input_weight*test_input(:,t)+internal_Weights*zeros(reservoirnodes,1);
            test_f=tanh(test_x0);
            test_f_1=tanh(test_x0-1);
            test_f1=tanh(test_x0+1);
            test_f_2=tanh(test_x0-2);
            test_f2=tanh(test_x0+2);
            output_reservoirtest(:,t)=w(1,:)'.*test_f+w(2,:)'.*test_f_1+w(3,:)'.*test_f1+w(4,:)'.*test_f_2+w(5,:)'.*test_f2;
        else
            test_x1=input_weight*test_input(:,t)+internal_Weights*output_reservoirtest(:,t-1);
            test_f=tanh(test_x1);
            test_f_1=tanh(test_x1-1);
            test_f1=tanh(test_x1+1);
            test_f_2=tanh(test_x1-2);
            test_f2=tanh(test_x1+2);